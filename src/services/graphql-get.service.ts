import { Injectable } from '@angular/core';
import { ApolloQueryResult } from '@apollo/client/core';
import {Apollo, ApolloBase, gql } from 'apollo-angular';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GraphqlGetService {

  private apollo: ApolloBase;

  constructor(
    private apolloProvider: Apollo
  ) { 
    this.apollo = this.apolloProvider.use('basicClient');
  }

  getData(): Observable<ApolloQueryResult<any>> {

    return this.apollo.query({
      query: gql`
        {
          rates(currency: "USD") {
            currency
            rate
          }
        }
      `,
    })
  }
  
}
