import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetServiceService {

  private url = 'https://jsonplaceholder.typicode.com';

  constructor(
    private http: HttpClient
  ) { }

  public getCommentsData(postId: number): Observable<any> {
    const commentsUrl: string = this.url + '/posts/' + postId + '/comments';
    return this.http.get(commentsUrl).pipe(
      tap( data => console.log(data) ),
      catchError(this.error)
    );
  }

  public getPostsData(): Observable<any> {
    const commentsUrl: string = this.url + '/posts';
    return this.http.get(commentsUrl).pipe(
      tap( data => console.log(data) ),
      catchError(this.error)
    );
  }

   // Method to handle HTTP errors
   public error(err: HttpErrorResponse): Observable<any> {

    let errorMessage = '';

    if (err.error instanceof ErrorEvent) {
      errorMessage = err.error.message;
    } else {
      errorMessage = `Error Code: ${err.status}\nMessage: ${err.message}`;
    }

    return throwError(errorMessage);

  }

}
