import { TestBed } from '@angular/core/testing';

import { GraphqlGetService } from './graphql-get.service';

describe('GraphqlGetService', () => {
  let service: GraphqlGetService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GraphqlGetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
