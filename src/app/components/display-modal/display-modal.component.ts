import { Component, OnInit } from '@angular/core';
import { Comment } from '../../../models/demo-models';
import { GetServiceService } from '../../../services/get-service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-display-modal',
  templateUrl: './display-modal.component.html',
  styleUrls: ['./display-modal.component.css']
})
export class DisplayModalComponent implements OnInit {

  public comments: Comment[] = [];
  public postId: number;
  public loadPage: boolean;

  constructor(
    private getService: GetServiceService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.loadPage = true;

    if (this.route.snapshot.params.postId) {
      this.postId = this.route.snapshot.params.postId;
      this.getService.getCommentsData(this.postId).subscribe((data: Comment[]) => {
        if (data) {
          this.comments = data;
          this.loadPage = false;
        }
      });
    }

  }

  public goToPosts(): void {
    this.router.navigate(['/posts']);
  }

}
