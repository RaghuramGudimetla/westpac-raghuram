import { Component, OnInit } from '@angular/core';
import { GraphqlGetService } from '../../../services/graphql-get.service';

@Component({
  selector: 'app-graphql-ui',
  templateUrl: './graphql-ui.component.html',
  styleUrls: ['./graphql-ui.component.css']
})
export class GraphqlUiComponent implements OnInit {

  constructor(
    private graphService: GraphqlGetService
  ) { }

  ngOnInit(): void {
    this.graphService.getData().subscribe((result) => {
      console.log(result.data);
    })
  }

}
