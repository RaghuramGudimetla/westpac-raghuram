import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphqlUiComponent } from './graphql-ui.component';

describe('GraphqlUiComponent', () => {
  let component: GraphqlUiComponent;
  let fixture: ComponentFixture<GraphqlUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraphqlUiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphqlUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
