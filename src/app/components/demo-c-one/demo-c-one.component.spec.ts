import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoCOneComponent } from './demo-c-one.component';

describe('DemoCOneComponent', () => {
  let component: DemoCOneComponent;
  let fixture: ComponentFixture<DemoCOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoCOneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoCOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
