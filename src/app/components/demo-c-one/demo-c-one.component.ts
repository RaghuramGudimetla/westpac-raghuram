import { Component, OnInit } from '@angular/core';
import { GetServiceService } from '../../../services/get-service.service';
import { Post } from '../../../models/demo-models';
import { Router } from '@angular/router';
import htmlToPdfmake from "html-to-pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts.js";
import pdfMake from 'pdfmake/build/pdfmake.js';

@Component({
  selector: 'app-demo-c-one',
  templateUrl: './demo-c-one.component.html',
  styleUrls: ['./demo-c-one.component.css']
})
export class DemoCOneComponent implements OnInit {

  public posts: Post[] = [];
  public selectedPost: number;
  public showComments = false;
  public loadPage: boolean;

  constructor(
    private getService: GetServiceService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadPage = true;
    this.getPosts();
  }
  private getPosts(): void {

    this.getService.getPostsData().subscribe((data) => {
      if (data && data.length) {
        this.posts = data;
        this.loadPage = false;
      }
    });

  }

  public showCommentOf(postId): void {
    this.router.navigate(['/comments/' + postId]);
  }

  public pdfDownload(): void {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    var html = htmlToPdfmake(`
    <p>
      This configuration has <strong>configuration</strong>, but not only.
    </p>
    `);

    var docDefinition = {
      content: [
        html
      ],
      styles: {
        'html-strong': {
          background: 'yellow'
        }
      }
    };
    pdfMake.createPdf(docDefinition).download('hh.pdf');
  }

}
