import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from '../app-routing.module';
import { DemoCOneComponent } from './components/demo-c-one/demo-c-one.component';
import { DisplayModalComponent } from './components/display-modal/display-modal.component';
import { GraphQLModule } from './graphql.module';
import {HttpLink} from 'apollo-angular/http';
import {InMemoryCache} from '@apollo/client/core';
import {APOLLO_NAMED_OPTIONS, NamedOptions} from 'apollo-angular';
import { GraphqlUiComponent } from './components/graphql-ui/graphql-ui.component';

@NgModule({
  declarations: [
    AppComponent,
    DemoCOneComponent,
    DisplayModalComponent,
    GraphqlUiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    GraphQLModule
  ],
  providers: [
    {
      provide: APOLLO_NAMED_OPTIONS,
      useFactory(httpLink: HttpLink): NamedOptions {
        return {
          basicClient: {
            cache: new InMemoryCache(),
            link: httpLink.create({
              uri: 'https://o5x5jzoo7z.sse.codesandbox.io/graphql',
            }),
          },
        };
      },
      deps: [HttpLink],
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
