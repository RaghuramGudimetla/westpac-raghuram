import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemoCOneComponent } from './app/components/demo-c-one/demo-c-one.component';
import { DisplayModalComponent } from './app/components/display-modal/display-modal.component';
import { GraphqlUiComponent } from './app/components/graphql-ui/graphql-ui.component';

// Defining Routes
const routes: Routes = [
  { path: '',  redirectTo: '/graphql', pathMatch: 'full' },
  { path: 'posts', component: DemoCOneComponent },
  { path: 'graphql', component: GraphqlUiComponent },
  { path: 'comments/:postId', component: DisplayModalComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
